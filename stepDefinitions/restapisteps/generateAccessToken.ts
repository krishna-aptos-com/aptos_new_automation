import {RestUtils} from '../../utils/restUtils';
require('../../hooks/restapihooks.ts');
const assert = require('assert');
const { Given , When, Then} = require('cucumber');
const RestUtilsobj: RestUtils = new RestUtils();
let response;

Given(/^post url "([^"]*)" request with input body is$/, async (url:string, inputbody:string) => {
   // response = await RestUtilsobj.postformbody(url, inputbody);

    console.log('KRISS '+ inputbody);

    const headerOptions: string = JSON.stringify({ 'Content-Type': 'application/x-www-form-urlencoded'});

    response = await RestUtilsobj.makeHttpRequest(url,  JSON.parse(headerOptions), 'POST' , inputbody, true);
    console.log('STATUS CODE' + response.statusCode);
    const json = JSON.parse(response.body);
    RestUtils.setAccessToken(json.access_token);
    console.log('ACCESS TOKEN' + RestUtils.getAccessToken());
    console.log(url + '\n');
    console.log(response);
    RestUtils.setStatusCode(response.statusCode);
});

Then(/^the status code must be "([^"]*)"$/, async (statusCode:string) => {
        assert.strictEqual(RestUtils.getStatusCode().toString(), statusCode.toString(), statusCode +
            'Status code is not equal' + RestUtils.getStatusCode() );
 });

When(/^user retrieves the access token"$/, async () => {

    assert.ok(RestUtils.getAccessToken().toString().length > 1, "Access token is not generated");
});
