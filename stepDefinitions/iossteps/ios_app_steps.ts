

import PlatformDomain from '../../pages/mobile_app/ios/platform_domain_screen';
import Login from '../../pages/mobile_app/ios/login_screen';
import Settings from '../../pages/mobile_app/ios/settings_screen';
import StoreAssociate from '../../pages/mobile_app/ios/store_associate_login_screen';
import Welcome from '../../pages/mobile_app/ios/welcome_screen';


const {Given, When, Then} = require('cucumber');
import {expect} from 'chai';

browser.timeouts('implicit',5000);

Given(/^User opens Aptos Store Selling iOS App and provided the Test Environment for Platform Domain as ([^"]*) and clicks on Done button$/, (platform_domain_env:string) =>{
  
        PlatformDomain.setPlatformDomain(platform_domain_env);

});

When(/^User provides valid credentials with ([^"]*) and ([^"]*) and clicks on Log in button$/, (username:string, password:string) =>
{
           Login.userLogin(username,password);
 
});

Then(/^should be able login successfully$/, () => {

        expect(Settings.isSettingsTitleDisplayed()).to.true;

  

});

When(/^User provides invalid credentials with ([^"]*) and ([^"]*) and clicks on Log in button$/, (username:string ,password:string) =>
{

            Login.userLogin(username,password);
 
});

Then(/^User should not be logged and should prompt an invalid username and password error message$/, () => {


            expect(Login.InvalidCredentialMessageIsDisplayed()).to.true;

            browser.reset();

});

Given(/^User Logged in successfully and verify the Setting page$/,() =>
{

          expect(Settings.isSettingsTitleDisplayed()).to.true;



});

When(/^User select Chicago retail Store and provides the Terminal Number as ([^"]*) and click on next transaction Number$/,(terminalNumber:string) =>
{

            Settings.clickOnSettingsDropdown();

            Settings.selectRetailStore();

            Settings.enterTerminalNumber(terminalNumber);

            Settings.clickOnNextTransactionNumber();
            
            Settings.clickOnSettingsDropdown();

            Settings.clickOnDone();

        //     if(Settings.isRevokeOtherDevicePopupDisplyed)
        //     {
        //             Settings.clickOnRevokeOtherDeviceYes();             
        //     }

  

});

Then (/^User should be able to redirect to Store Associate Login page$/, () =>
{

        browser.pause(1000);

        StoreAssociate.dismissAll();
            

            expect(StoreAssociate.isUsernameEditBoxDisplayed()).to.true;

  

});

When(/^User provide Invalid Store Associate credentials as ([^"]*) and ([^"]*)$/,(username:string , password:string) => {

                
             StoreAssociate.login(username,password);


});

Then(/^User should not able to login and should show invalid username or password alert popup$/, () => {

          expect(StoreAssociate.isInvalidUsernameOrPasswordPopupDisplayed()).to.true;

          StoreAssociate.closeInvalidUsernameOrPasswordPopup();



});

When(/^User provide valid Store Associate credentials as ([^"]*) and ([^"]*)$/,(username:string , password:string) => {

           StoreAssociate.login(username,password);


});

Then(/^User should be logged in with logged in username gretting message$/, () => {

           expect(Welcome.isWelcomeMessageDisplayed()).to.true;

           browser.pause(5000);
});

