

class SettingScreen {


    private get settings_lable()
    {
        return $(`android=new UiSelector().text("Settings")`);
    };

    private get settings_dropdown()
    {
        return $(`android=new UiSelector().text("Select Retail Store")`);
    };

    // private get retail_store()
    // {
    //     return $(`android=new UiSelector().text("Chicago, Aptos Store")`);
    // }

    private get chicago_retail_Store()
    {
        return $('//android.view.ViewGroup/android.view.ViewGroup[2]');
        //return $(`android=new UiSelector().text("Chicago, Aptos Store").className("android.view.ViewGroup")`);
    }

    private get terminal_number_edit_box()
    {
        return $(`android=new UiSelector().text("Enter terminal number")`);
    }

    private get next_transaction_number_edit_box()
    {
        return $(`android=new UiSelector().text("Enter next transaction Number")`);
    }

    private get done_button()
    {
        return $(`android=new UiSelector().text("Done >")`);
    }

    private get revoke_other_device_popup_text()
    {
        return $(`android=new UiSelector().text("Do you really want to revoke the other device and register this device?")`);
    }

    private get revoke_other_device_yes_btn()
    {
        return $(`android=new UiSelector().text("Yes")`);
    }


    private get revoke_other_device_no_btn()
    {
        return $(`android=new UiSelector().text("No")`);
    }

    public isSettingsTitleDisplayed():boolean
    {
        return this.settings_lable.isExisting();
    }

    public clickOnSettingsDropdown()
    {
        this.settings_dropdown.click();
    }
    
    public selectRetailStore()
    {
           // this.retail_store.click();
            this.chicago_retail_Store.click();
    }

    public enterTerminalNumber(terminal_number:string)
    {
        this.terminal_number_edit_box.setValue(terminal_number);
    }

    public clickOnNextTransactionNumber()
    {
        this.next_transaction_number_edit_box.click();


        //this.next_transaction_number_edit_box.setValue('\n');
    }

    public clickOnDone()
    {
       
        this.done_button.click();


    }

    public isRevokeOtherDevicePopupDisplyed():boolean
    {
        return this.revoke_other_device_popup_text.isExisting();
    }

    public clickOnRevokeOtherDeviceYes()
    {
        this.revoke_other_device_yes_btn.click();
    }

    public clickOnRevokeOtherDeviceNo()
    {
        this.revoke_other_device_no_btn.click();
    }

}

const Settings = new SettingScreen();

export default Settings;