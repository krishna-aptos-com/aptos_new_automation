
class StoreAssociateLoginScreen {

  

    private get username_edit_box()
    {
        
      // return $(`android=new UiSelector().text("Username")`);

      return $('//android.view.ViewGroup/android.widget.EditText[1]');

    };

    private get password_edit_box()
    {
        return $('//android.view.ViewGroup/android.widget.EditText[2]');
        
        //return $(`android=new UiSelector().text("Password")`);
        
    }

    private get invalid_username_or_password_popup()
    {
        return $(`android=new UiSelector().text("Invalid username or password")`);
    }

    private get close_invalid_credential_popup()
    {
        return $(`android=new UiSelector().text("Close")`);
    }

    public isUsernameEditBoxDisplayed():boolean
    {
      

        return this.username_edit_box.isExisting();
    }

    private setUsername(username:string)
    {
        
        this.username_edit_box.setValue(username);
       
    }

    private setPassword(password:string)
    {
        
        this.password_edit_box.setValue(password + '\n');
        
    }

    public login(username:string, password:string)
    {
    
        this.setUsername(username);

        this.setPassword(password);
    }

    public isInvalidUsernameOrPasswordPopupDisplayed():boolean
    {
        return this.invalid_username_or_password_popup.isExisting();
    }

    public closeInvalidUsernameOrPasswordPopup()
    {
        this.close_invalid_credential_popup.click();
    }


}

const StoreAssociate = new StoreAssociateLoginScreen();

export default StoreAssociate;