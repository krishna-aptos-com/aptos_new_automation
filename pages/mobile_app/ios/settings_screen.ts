

class SettingScreen {


    private get settings_dropdown()
    {
        return $('~Settings');
    };

    private get retail_store()
    {
        return $('(//XCUIElementTypeOther[@name="Select Retail Store"])[3]');
    }

    private get chicago_retail_Store()
    {
        return $('//XCUIElementTypeApplication[@name="MStore Debug"]/XCUIElementTypeWindow[1]/XCUIElementTypeOther[2]/XCUIElementTypeOther');
    }

    // private get atlanta_retail_Store()
    // {
    //     return $('(//XCUIElementTypeOther[@name="Atlanta, GA Shops Around Lenox"])[2]');
    // }

    private get terminal_number_edit_box()
    {
        return $('(//XCUIElementTypeOther[@name="Enter terminal number"])[2]/XCUIElementTypeTextField');
    }

    private get next_transaction_number_edit_box()
    {
        return $('(//XCUIElementTypeOther[@name="Enter next transaction Number"])[2]/XCUIElementTypeTextField');
    }

    // private get next_transaction_number_value()
    // {
    //     return $('(//XCUIElementTypeOther[@name="Settings Aptos Denim Chicago, Aptos Store"])[3]/XCUIElementTypeOther[4]/XCUIElementTypeOther/XCUIElementTypeTextField')
    // }

    private get done_button()
    {
        return $('~Done >');
    }

    private get revoke_other_device_popup_text()
    {
        return $('//XCUIElementTypeOther[@name="Do you really want to revoke the other device and register this device?"]');
    }

    private get revoke_other_device_yes_btn()
    {
        return $('~Yes');
    }

    private get revoke_other_device_no_btn()
    {
        return $('~No');
    }

    public isSettingsTitleDisplayed():boolean
    {
        return this.settings_dropdown.isExisting();
    }

    public clickOnSettingsDropdown()
    {
        this.settings_dropdown.click();
    }
    
    public selectRetailStore()
    {
            this.retail_store.click();
            this.chicago_retail_Store.click();
        
    }

    public enterTerminalNumber(terminal_number:string)
    {
        this.terminal_number_edit_box.setValue(terminal_number);
    }

    public clickOnNextTransactionNumber()
    {


        this.next_transaction_number_edit_box.click();


    }

    public clickOnDone()
    {
        this.done_button.click();
    }

    public isRevokeOtherDevicePopupDisplyed():boolean
    {
        return this.revoke_other_device_popup_text.isExisting();
    }

    public clickOnRevokeOtherDeviceYes()
    {
        this.revoke_other_device_yes_btn.click();
    }

    public clickOnRevokeOtherDeviceNo()
    {
        this.revoke_other_device_no_btn.click();
    }
}

const Settings = new SettingScreen();

export default Settings;