export class LogInPageObjects {
    public static get loginLink() {
        return browser.element("//span[contains(.,'Log In')]");
    }

    // entering user Name
    public static get userNameTextbox() {
        return browser.element("//*[@id='username']");
    }

    // Entering pass word
    public static get passwordTextbox() {
        return browser.element("//*[@id='password']");
    }

    // clicks on login button
    public static get logInButton() {
        return browser.element("//input[@name='login']");
    }

    public static get loginErrorLabel(){
        return browser.element("//*[@class='kc-feedback-text']");
    }
}
