
import {IContentObjectImpl, TestRailreport} from '../reporthelper/testrailUtil';


const { Before, BeforeAll, After, AfterAll, Status} = require('cucumber');

const testrail = new TestRailreport();
const PropertiesReader = require('properties-reader');
const properties = PropertiesReader('./config/testrail.properties');

BeforeAll({timeout: 100 * 3000}, async () => {
    console.log('Before ALL');
   let runName: string = properties.get('runName') ;
   await  testrail.createTestrun(runName, properties.get('suitId'));

});

After({timeout: 100 * 3000}, async function(scenario) {
    console.log('SCEnario ' + scenario.result.status.toString());
    const caseids = scenario.pickle.name.split(' ');
    console.log('CASE IDS --------------' + caseids[0].charAt(1));
    const content = new IContentObjectImpl();
    content.assignedto_id ='1';
    content.comment = 'update result';
    content.version = properties.get('version');
    content.defects = 'APT459';

    if (scenario.result.status === Status.FAILED) {
        content.status_id = 5;
        console.log('TEST CASE STATUS  '+ Status.PASSED.valueOf());
    } else if (scenario.result.status === Status.PASSED){
        content.status_id = 1;
        Status.PASSED.valueOf()
        console.log('TEST CASE STATUS '+ content.status_id);
    }
   const runid = await testrail.getRunId(properties.get('runName'));

    console.log('RUNIDKR '+ runid);
    await testrail.addTestResult(runid, caseids[0].charAt(1), content);

});

/*Below code for future reference */
AfterAll({timeout: 100 * 1000}, async () => {
 //   await browser.quit();
});

Before(async function() {
    // This hook will be executed before all scenarios
});

Before({tags: '@foo'}, function() {
    // This hook will be executed before scenarios tagged with @foo
});

Before({tags: '@foo and @bar'}, function() {
    // This hook will be executed before scenarios tagged with @foo and @bar
});

Before({tags: '@foo or @bar'}, function() {
    // This hook will be executed before scenarios tagged with @foo or @bar
});

// You can use the following shorthand when only specifying tags
Before('@foo', function() {
    // This hook will be executed before scenarios tagged with @foo
});

