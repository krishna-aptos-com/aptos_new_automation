const path = require('path');

const report = require('cucumber-html-reporter');


let options = {
    theme: 'bootstrap',
    jsonFile: 'report/cucumber_report.json',
    output: './cucumber_report.html',
    reportSuiteAsScenarios: true,
    launchReport: true,
    metadata: {
        "App Version":"0.3.2",
        "Test Environment": "STAGING",
        "Browser": "Chrome  54.0.2840.98",
        "Platform": "MACOS",
        "Parallel": "Scenarios",
        "Executed": "Remote"
    }
};

report.generate(options);
